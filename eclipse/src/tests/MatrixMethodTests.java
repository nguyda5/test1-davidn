//David Nguyen, 1938087

package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import utilities.MatrixMethod;

class MatrixMethodTests {

	MatrixMethod matMethods = new MatrixMethod();
	
	@Test
	void testDuplicate() {
		
		int[][] test1 = {{1,2,3},{4,5,6}};
		int[][] test2 = {{-1},{-2},{4}};
		
		int[][] expectedOutput1 = {{1,2,3,2,3},{4,5,6,4,5,6}};
		int[][] expectedOutput2 = {{-1,-1},{-2,-2},{4,4}};
		
		assertArrayEquals(expectedOutput1,matMethods.duplicate(test1));
		assertArrayEquals(expectedOutput2,matMethods.duplicate(test2));
	}

}
