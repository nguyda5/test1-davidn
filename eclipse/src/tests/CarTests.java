//David Nguyen, 1938087

package tests;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import vehicles.Car;

class CarTests {
	
	Car testCar = new Car(5);
	
	@Test
	void testGetSpeed() {
		assertEquals(5,testCar.getSpeed());
	}
	
	@Test
	void testGetLocation() {
		assertEquals(0,testCar.getLocation());
	}
	
	@Test
	void testMoveRight() {
		testCar.moveRight();
		testCar.moveRight();
		testCar.moveRight();
		assertEquals(15,testCar.getLocation());
	}
	
	@Test
	void testMoveLeft() {
		testCar.moveLeft();
		testCar.moveLeft();
		testCar.moveLeft();
		assertEquals(-15,testCar.getLocation());
	}

	@Test
	void testAccelerate() {
		testCar.accelerate();
		testCar.accelerate();
		testCar.accelerate();
		testCar.accelerate();
		assertEquals(9, testCar.getSpeed());
	}
	
	@Test
	void testDecelerate() {
		testCar.decelerate();
		testCar.decelerate();
		testCar.decelerate();
		testCar.decelerate();
		
		assertEquals(1,testCar.getSpeed());
		
		testCar.decelerate();
		testCar.decelerate();
		testCar.decelerate();
		
		assertEquals(0,testCar.getSpeed());
	}
	
	@Test
	void testExceptionNegativeSpeed() {
		try {
			Car carTest2 = new Car(-2);
			
		fail("Constructor was supposed to throw an exception, but did not!");
		}
		catch (IllegalArgumentException e) {
			
		}
	}
}
