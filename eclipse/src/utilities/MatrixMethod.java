//David Nguyen, 1938087

package utilities;

public class MatrixMethod {
	
	//Returns a duplicate of a matrix
	public int[][] duplicate(int[][] givenArray){
		
		int dupLength = givenArray.length;
		int dupLength2 = givenArray[0].length*2;
		
		int[][] dupArray = new int[dupLength][dupLength2]; 
		
		
		//Outer loops and inner loops to cycle through the 2D array
		for(int j = 0; j < dupArray.length; j++) {
			int givenArrayTrack = 0;
			for(int i = 0; i < dupArray[0].length; i++) {
				
				dupArray[j][i] = givenArray[j][givenArrayTrack];
				givenArrayTrack++;
				
				
				//Once the loop has reached the last number of j in the original array, it resets the tracker so we can duplicate
				if(givenArrayTrack == givenArray[0].length) {
					givenArrayTrack = 0;
				}
				
			}
		}
		
		return dupArray;
	}
}
