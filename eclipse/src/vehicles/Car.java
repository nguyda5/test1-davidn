package vehicles;

public class Car {
	private int speed;
	private int location;
	
	/**
	 * Creates an initializes a Car object.
	 * @param speed How fast the car initially goes. Must be >= 0
	 */
	public Car(int speed) {
		if (speed < 0) {
			throw new IllegalArgumentException("Speed must not be negative!");
		}
		
		this.speed = speed;
	}
	
	/**
	 * Gets the current speed of the Car.
	 * @return Gets the value of the speed field.
	 */
	public int getSpeed() {
		return this.speed;
	}
	
	/**
	 * Gets the location of the Car.
	 * @return Gets the value of the location field.
	 */
	public int getLocation() {
		return this.location;
	}
	
	/**
	 * Moves the Car to the right by adding the speed.
	 */
	public void moveRight() {
		this.location = this.location + this.speed;
	}
	
	/**
	 * Moves the Car to the left by subtracting the speed.
	 */
	public void moveLeft() {
		this.location = this.location - this.speed;
	}
	
	/**
	 * Increases the speed by 1 unit.
	 */
	public void accelerate() {
		this.speed++;
	}
	
	/**
	 * If the Car is moving (i.e. has a speed > 0), then decreases the speed by 1 unit. Otherwise there is no change.
	 */
	public void decelerate() {
		if (this.speed > 0) {
			this.speed--;
		}
	}
}
